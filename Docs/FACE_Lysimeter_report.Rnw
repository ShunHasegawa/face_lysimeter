\documentclass[11pt]{article}

\usepackage{fullpage}
\usepackage{pdfpages}
\usepackage{float}

\title{FACE lysimeter results -Summary Stats-}
\author{Shun Hasegawa}
\date{\today}

\begin{document}

<<setup, include=FALSE>>=
opts_chunk$set(concordance = TRUE, warning = FALSE, tidy = TRUE, tidy.opts = list(width.cutoff = 60))
opts_knit$set(root.dir=normalizePath('../'))
@


<<readFiles, include=FALSE>>=
library(car)
library(gmodels)
library(lme4)
library(lubridate)
library(MASS)
library(nlme)
library(packrat)
library(plyr)
library(reshape)
library(xlsx)
library(contrast)
library(effects)
library(ggplot2)
library(scales)
library(xtable)
library(gridExtra)


source("R//functions.R")
load("output//data//FACE_lysimeter.Rdata")
source("R//SummaryExlTable.R")
source("R//Figs.R")
@

\maketitle
\clearpage
\tableofcontents
\clearpage

%%%
%%%
%%%

\section{Lysimeter}

%%%%%%%%%%%%%%%
% summary fig %
%%%%%%%%%%%%%%%
\subsection{Summary results}
\begin{center}
\begin{figure}[!ht]\label{figure:FACE_Lysimeter_Allres}

\includegraphics[width=\textwidth]{../output/figs/FACE_LysimeterCO2.pdf}

\caption{Dissoluved nutrients in soil water}

\end{figure}
\end{center}

%%%%%%%%%%%
% Nitrate %
%%%%%%%%%%%
\Sexpr{knit_child('FACE_Lysimeter_nitrate.Rnw')}

%%%%%%%%%%%%
% Ammonium %
%%%%%%%%%%%%
\clearpage
\Sexpr{knit_child('FACE_Lysimeter_Ammonium.Rnw')}

%%%%%%%%%%%%%
% Phosphate %
%%%%%%%%%%%%%
\clearpage
\Sexpr{knit_child('FACE_Lysimeter_Phosphate.Rnw')}

%%%%%%%
% TOC %
%%%%%%%
\clearpage
\Sexpr{knit_child('FACE_Lysimeter_TOC.Rnw')}

%%%%%%
% TC %
%%%%%%
\clearpage
\Sexpr{knit_child('FACE_Lysimeter_TC.Rnw')}

%%%%%%
% IC %
%%%%%%
\clearpage
\Sexpr{knit_child('FACE_Lysimeter_IC.Rnw')}

%%%%%%
% TN %
%%%%%%
\clearpage
\Sexpr{knit_child('FACE_Lysimeter_TN.Rnw')}


%%%%%%%%%%%%%%%%
% List of figs %
%%%%%%%%%%%%%%%%
\clearpage
\listoffigures

\end{document}